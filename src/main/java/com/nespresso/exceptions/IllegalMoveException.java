package com.nespresso.exceptions;

public class IllegalMoveException extends RuntimeException {
	public IllegalMoveException(String message) {
		super(message);
	}

}
