package com.nespresso.sofa.recruitment.labyrinth;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.nespresso.room.RoomDetails;
import com.nespresso.walker.Walker;
import com.nespresso.walker.WalkerHelper;

public class Labyrinth {

	private Map<String,Walker> walker = new HashMap<>();
	
	public Labyrinth(String... walkers) {
		for(String walkerCombination:walkers)
		{
			
			String[] walkerNames=walkerCombination.split("[$,|]");
			String symbol = walkerCombination.replaceAll("[a-zA-z]","");
			Walker firstWalker = new Walker(walkerNames[0]);
			Walker secondWalker = new Walker(walkerNames[1]);
			RoomDetails roomDetails = new RoomDetails();
			Iterator<String> itr = walker.keySet().iterator();
			while(itr.hasNext())
			{
				String walkerName = itr.next();
				if(walkerName.equals(walkerNames[0]))
				{
					firstWalker=walker.get(walkerName);
					
				}else if(walkerName.equals(walkerNames[1]))
				{
					secondWalker=walker.get(walkerName);
				}
			}
			roomDetails.setHavingSensor(symbol);
			firstWalker.getWalkers().put(secondWalker.getShortName(),roomDetails);
			secondWalker.getWalkers().put(firstWalker.getShortName(), roomDetails);
			walker.put(firstWalker.getShortName(), firstWalker);
			walker.put(secondWalker.getShortName(), secondWalker);
			
		}
		WalkerHelper.getInstance().setWalker(walker);
	}


	public Walker popIn(String string) {
		
		return walker.get(string);
	}


	public BigDecimal readSensors() {
		// TODO Auto-generated method stub
		return null;
	}

}
