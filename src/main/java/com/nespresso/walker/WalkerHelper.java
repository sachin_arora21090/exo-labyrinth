package com.nespresso.walker;

import java.util.Map;

import com.nespresso.exceptions.IllegalMoveException;

public class WalkerHelper {
	private Map<String,Walker> walker;
	private static WalkerHelper helper = new WalkerHelper();

	private WalkerHelper(){}
	
	public static WalkerHelper getInstance()
	{
	return helper;
	}
	public Map<String, Walker> getWalker() {
		return walker;
	}

	public void setWalker(Map<String, Walker> walker) {
		this.walker = walker;
	}
	
	public Walker checkRoom(String room)
	{
        Walker presentWalker =  WalkerHelper.getInstance().getWalker().get(room);
	    
		if(presentWalker == null)
		{
			throw new IllegalMoveException("Room does not exist in Labyrinth");
		}
		return presentWalker;
	}
	
	

}
