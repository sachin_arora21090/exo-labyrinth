package com.nespresso.walker;

import java.util.HashMap;
import java.util.Map;

import com.nespresso.exceptions.IllegalMoveException;
import com.nespresso.room.RoomDetails;

public class Walker {

	private Map<String, RoomDetails> walkers = new HashMap<>();
	private RoomDetails presentWalkerName;
	private Walker presentWalker;
	private Walker oldWalker;
	private String shortName;
	boolean firstTime=true;

	public Walker(String shortName) {
		this.shortName = shortName;
	}

	public String getShortName() {
		return shortName;
	}
	public Map<String, RoomDetails> getWalkers() {
		return walkers;
	}

	public void setWalkers(Map<String, RoomDetails> walkers) {
		this.walkers = walkers;
	}

	public void walkTo(String room) {
		if(firstTime)
		{
			firstTime=false;
	         presentWalker = WalkerHelper.getInstance().checkRoom(room);
		}
		else
		{
			oldWalker=presentWalker;
		    presentWalker = WalkerHelper.getInstance().checkRoom(room);
			if(oldWalker.getWalkers().get(room) == null)
			{
				throw new IllegalMoveException("No gate for this room"+room);
			}
		}

	}

	public String position() {

		return presentWalker.getShortName();
	}

	public void closeLastDoor() {
		


	}

	public String toString() {
		return shortName;
	}

}
