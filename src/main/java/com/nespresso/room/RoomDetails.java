package com.nespresso.room;

public class RoomDetails {
	
	private boolean havingSensor;
	private boolean doorClosed;

	public boolean isDoorClosed() {
		return doorClosed;
	}

	public void setDoorClosed(boolean doorClosed) {
		this.doorClosed = doorClosed;
	}

	public boolean isHavingSensor() {
		return havingSensor;
	}

	public void setHavingSensor(String symbol) {
		
		if(symbol.contains("$"))
		{
			this.havingSensor =true;
		}else{
		
			this.havingSensor=false;
		}
	}
	
	public String toString()
	{
		return ""+havingSensor;
	}

}
